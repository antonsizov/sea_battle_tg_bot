package main

import (
	"fmt"
	"math/rand"
	"strings"
	"errors"
	"strconv"

	"github.com/nu7hatch/gouuid"
)

//
// GLOBALS
//

var gamesMap map[GameId] *Game
var waitForRandomPlayer *UserId

//
// TYPES
//

const BOT_USER_ID UserId = -1

type UserGames map[UserId] *UserGame
type UserGame struct {
	Field Field
	Ships Ships
}

type Field map[CellId] *Cell
type CellId string
type Cell struct {
	State int
	Ship ShipId // "" - in case no ship
	IsNeerShip bool
}

type ShotResult int

const (
	GAME_OVER_SHOT_RESULT ShotResult = 0
	OVERSHOT_SHOT_RESULT ShotResult = 1
	HIT_SHIP_SHOT_RESULT ShotResult = 2
	SANK_SHIP_SHOT_RESULT ShotResult = 3
)

type GameId string
type Game struct {
	id GameId
	Users UserGames
	Turn UserId
}

type Ships map[ShipId] *Ship
type ShipId string
type Ship struct {
	Id ShipId
	Size int // 1-4
	Hits int // 0-4
}

const (
	WAIT_FOR_SHOT_CELL_STATE = 0
	OVERSHOT_CELL_STATE = 1
	HIT_SHIP_CELL_STATE = 2
)

//
// GAME METHODS
//

func newGame(userId1, userId2 UserId) *Game {
	u, err := uuid.NewV4()
	if err != nil {
		panic(err)
	}
	uuidString := u.String()
	gameId := GameId(uuidString)

	games := make(map[UserId] *UserGame, 2)
	userGame1 := &UserGame{
		Field: newField(),
		Ships: newShips(),
	}
	userGame2 := &UserGame{
		Field: newField(),
		Ships: newShips(),
	}
	games[userId1] = userGame1
	games[userId2] = userGame2

	return &Game {
		id: gameId,
		Users: games,
		Turn: userId1,
	}
}

func (g *Game) getOpponentUserId(userId UserId) UserId {
	var opponentUserId UserId
	for opponentUserId, _ = range g.Users {
		if opponentUserId != userId {
			break
		}
	}
	return opponentUserId
}

func (g *Game) isUserTurn(userId UserId) bool {
	return g.Turn == userId
}

func (g *Game) makeBotShot() (ShotResult, CellId) {
	userId := g.getOpponentUserId(BOT_USER_ID)
	userGame := g.Users[userId]
	cellId := userGame.Field.getRandomWaitForShotCoordinates()
	shotResult := handleShot(userGame, cellId)
	if shotResult == OVERSHOT_SHOT_RESULT {
		g.Turn = userId
	}
	// TODO: validate shotResult [0,1,2]
	return shotResult, cellId
}

func (g *Game) makeUserShot(userId UserId, letterIdx, numberIdx int) ShotResult {
	cellId := getCellId(letterIdx, numberIdx)
	opponentUserId := g.getOpponentUserId(userId)
	opponentGame := g.Users[opponentUserId]
	switch handleShot(opponentGame, cellId) {
	case OVERSHOT_SHOT_RESULT:
		g.Turn = opponentUserId
		return OVERSHOT_SHOT_RESULT
	case HIT_SHIP_SHOT_RESULT:
		g.Turn = userId
		return HIT_SHIP_SHOT_RESULT
	case SANK_SHIP_SHOT_RESULT:
		g.Turn = userId
		return SANK_SHIP_SHOT_RESULT
	case GAME_OVER_SHOT_RESULT:
		return GAME_OVER_SHOT_RESULT
	default:
		panic("unexpected return value")
	}
	return OVERSHOT_SHOT_RESULT // never goes here
}

func (g *Game) isOpponentCellAvailableForShot(userId UserId, letterIdx, numberIdx int) bool {
	opUserId := g.getOpponentUserId(userId)
	opGame:= g.Users[opUserId]
	opField := opGame.Field
	cellId := getCellId(letterIdx, numberIdx)
	return opField.isAvailableForShot(cellId)
}

func getLetterIdx(char string) (int, error) {
	char = strings.ToUpper(char)
	letterIds := map[string]int{
		"A": 0,
		"B": 1,
		"C": 2,
		"D": 3,
		"E": 4,
		"F": 5,
		"G": 6,
		"H": 7,
		"I": 8,
		"J": 9,
	}
	letterIdx, ok := letterIds[char]
	if !ok {
		return -1, errors.New(fmt.Sprintf("Char %v not in range", char))
	}
	return letterIdx, nil
}

func (g *Game) setUserShips() {
	for userId, _ := range g.Users {
		g.randomlySetUserShips(userId)
	}
	return
}

func (g *Game) randomlySetUserShips(userId UserId) int {
	userGame := g.Users[userId]
	for _, ship := range userGame.Ships {
		randomlySetShip(ship, &userGame.Ships, &userGame.Field)
	}
	return 0
}

func (g *Game) getUserFieldASCIIState(userId UserId) string {
	userGame := g.Users[userId]
	field := userGame.Field
	return field.getOwnerFieldAsciiState()
}

func (g *Game) getOpponentFieldASCIIState(userId UserId) string {
	opponentUserId := g.getOpponentUserId(userId)
	return g.getUserFieldASCIIState(opponentUserId)
}

func (g *Game) getUserShotsASCIIMap(userId UserId) string {
	opponentUserId := g.getOpponentUserId(userId)
	opponentGame := g.Users[opponentUserId]
	field := opponentGame.Field
	return field.getShotsASCIIMap()
}

//
// FIELD METHODS
//

func newField() Field{

	var letterI, numI int
	field := make(map[CellId] *Cell, 100)
	for letterI = 0; letterI < 10; letterI++ {
		for numI = 0; numI < 10; numI++ {
			cellId := getCellId(letterI, numI)
			field[cellId] = newCell()
		}
	}
	return field
}

func (f *Field) getRandomWaitForShotCoordinates() CellId {

	var cell *Cell
	var cellId CellId

	numWaitForShotCoordinates := 0
	for _, cell = range (*f) {
		if cell.waitForShotState() {
			numWaitForShotCoordinates++
		}
	}

	n := rand.Intn(numWaitForShotCoordinates)

	for cellId, cell = range (*f) {
		if cell.waitForShotState() {
			if n == 0 {
				break
			}
			n--
		}
	}

	return cellId
}

func (f *Field) setNeerbyCellsUnavailableForShot(shipId ShipId) int {
	for cellId, cell := range (*f) {

		// skip this `for` iteration in case cell is not contain necessary ship
		if cell.Ship != shipId {
			continue
		}

		letterI := cellId.getLetterIdx()
		numI := cellId.getNumberIdx()

		// in case cell neerby set coordinates valid (in within field)
		// and cell state is wait for shot - set cell state to - overshot
		// up
		var neerbyCell *Cell
		if coordinatesValid(letterI, numI-1) {
			neerbyCell = f.getCell(letterI, numI-1)
			if neerbyCell.waitForShotState() {
				neerbyCell.setOvershotState()
			}
		}
		// up right
		if coordinatesValid(letterI+1, numI-1) {
			neerbyCell = f.getCell(letterI+1, numI-1)
			if neerbyCell.waitForShotState() {
				neerbyCell.setOvershotState()
			}
		}
		// right
		if coordinatesValid(letterI+1, numI) {
			neerbyCell = f.getCell(letterI+1, numI)
			if neerbyCell.waitForShotState() {
				neerbyCell.setOvershotState()
			}
		}
		// right down
		if coordinatesValid(letterI+1, numI+1) {
			neerbyCell = f.getCell(letterI+1, numI+1)
			if neerbyCell.waitForShotState() {
				neerbyCell.setOvershotState()
			}
		}
		// down
		if coordinatesValid(letterI, numI+1) {
			neerbyCell = f.getCell(letterI, numI+1)
			if neerbyCell.waitForShotState() {
				neerbyCell.setOvershotState()
			}
		}
		// down left
		if coordinatesValid(letterI-1, numI+1) {
			neerbyCell = f.getCell(letterI-1, numI+1)
			if neerbyCell.waitForShotState() {
				neerbyCell.setOvershotState()
			}
		}
		// left
		if coordinatesValid(letterI-1, numI) {
			neerbyCell = f.getCell(letterI-1, numI)
			if neerbyCell.waitForShotState() {
				neerbyCell.setOvershotState()
			}
		}
		// up left
		if coordinatesValid(letterI-1, numI-1) {
			neerbyCell = f.getCell(letterI-1, numI-1)
			if neerbyCell.waitForShotState() {
				neerbyCell.setOvershotState()
			}
		}
	}
	return 0
}

func (f *Field) setShip(ship *Ship, letterI, numI, direction int) int {
	for i := ship.Size; i >= 1; i-- {
		cell := f.getCell(letterI, numI)
		cell.Ship = ship.Id

		// set neer_ship flags
		// up
		if coordinatesValid(letterI, numI-1) {
			cell = f.getCell(letterI, numI-1)
			cell.setNeerShip()
		}
		// up right
		if coordinatesValid(letterI+1, numI-1) {
			cell = f.getCell(letterI+1, numI-1)
			cell.setNeerShip()
		}
		// right
		if coordinatesValid(letterI+1, numI) {
			cell = f.getCell(letterI+1, numI)
			cell.setNeerShip()
		}
		// right down
		if coordinatesValid(letterI+1, numI+1) {
			cell = f.getCell(letterI+1, numI+1)
			cell.setNeerShip()
		}
		// down
		if coordinatesValid(letterI, numI+1) {
			cell = f.getCell(letterI, numI+1)
			cell.setNeerShip()
		}

		// down left
		if coordinatesValid(letterI-1, numI+1) {
			cell = f.getCell(letterI-1, numI+1)
			cell.setNeerShip()
		}

		// left
		if coordinatesValid(letterI-1, numI) {
			cell = f.getCell(letterI-1, numI)
			cell.setNeerShip()
		}

		// up left
		if coordinatesValid(letterI-1, numI-1) {
			cell = f.getCell(letterI-1, numI-1)
			cell.setNeerShip()
		}

		nextXY(&letterI, &numI, direction)
	}
	return 0
}

func (f *Field) isFree(letterI, numI, direction, shipSize int) bool {
	for i := shipSize; i >= 1; i-- {
		if f.isCellFreeAndValid(letterI, numI) {
			nextXY(&letterI, &numI, direction)
		} else {
			return false
		}
	}
	return true
}

func nextXY(x, y *int, direction int) int {
	if direction == 0 { // up
		*y--
	} else if direction == 1 { // right
		*x++
	} else if direction == 2 { // down
		*y++
	} else { // 3 - left
		*x--
	}
	return 0
}

func (f *Field) getOwnerFieldAsciiState() string {
	ascii := ">>> A B C D E F G H I J \n"
	for numI := 0; numI < 10; numI++ {
		ascii = ascii + emojiNumbers[numI]
		for letterI := 0; letterI < 10; letterI++ {
			cellId := getCellId(letterI, numI)
			cell := (*f)[cellId]

			if cell.overshotState() {
				ascii = ascii + overshotCellSymbol
			} else if cell.hitState() {
				ascii = ascii + hitCellSymbol
			} else if !cell.isEmpty() {
				ascii = ascii + shipCellSymbol
			} else if cell.waitForShotState() {
				ascii = ascii + waitForShotCellSymbol
			}
		}
		ascii = ascii + "\n"
	}
	return ascii
}

func (f *Field) getShotsASCIIMap() string {
	ascii := ">>> A >B >C >D >E >F >G >H >I >J \n"
	for numI := 0; numI < 10; numI++ {
		ascii = ascii + emojiNumbers[numI]
		for letterI := 0; letterI < 10; letterI++ {
			cellId := getCellId(letterI, numI)
			cell := (*f)[cellId]
			if cell.waitForShotState() {
				ascii = ascii + waitForShotCellSymbol
			} else if cell.overshotState() {
				ascii = ascii + overshotCellSymbol
			} else if cell.hitState() {
				ascii = ascii + hitCellSymbol
			}
		}
		ascii = ascii + "\n"
	}
	return ascii
}

func (f *Field) isCellFreeAndValid(letterI int, numI int) bool {
	if (letterI < 0 || letterI > 9 || numI < 0 || numI > 9) {
		return false
	}
	cell := f.getCell(letterI, numI)
	if cell.isEmpty() && !cell.isNeerShip() {
		return true
	}
	return false
}

func (f *Field) getCell2(cellId CellId) *Cell {
	return (*f)[cellId]
}

func (f *Field) getCell(letterI, numI int) *Cell {
	cellId := getCellId(letterI, numI)
	return (*f)[cellId]
}

func getCellId(cellLetterId int, cellNumId int) CellId {
	return CellId(coordinatesToString(cellLetterId, cellNumId))
}

func handleShot(userGame *UserGame, cellId CellId) ShotResult {
	ships := userGame.Ships
	field := userGame.Field
	// for debug only
	opField := field.getOwnerFieldAsciiState()
	fmt.Println("opponent field:\n", opField)

	var result ShotResult
	cell := field.getCell2(cellId)
	if !cell.waitForShotState() {
		panic(fmt.Sprintf("cell not available for shot: ", cellId))
	}
	if cell.isEmpty() {
		cell.setOvershotState()
		result = OVERSHOT_SHOT_RESULT
	} else {
		cell.setHitState()
		ship := ships[cell.Ship]
		ship.Hits++
		if ship.isSank() {
			field.setNeerbyCellsUnavailableForShot(ship.Id)
			result = SANK_SHIP_SHOT_RESULT
		} else {
			result = HIT_SHIP_SHOT_RESULT
		}
		if isAllSank(ships) {
			result = GAME_OVER_SHOT_RESULT
		}
	}
	return result
}

func (f *Field) isAvailableForShot(cellId CellId) bool {
	cell := f.getCell2(cellId)
	return cell.waitForShotState()
}

//
// CELL METHODS
//

func (ci *CellId) getLetterIdx() int {
	letterIds := map[string]int{
		"A": 0,
		"B": 1,
		"C": 2,
		"D": 3,
		"E": 4,
		"F": 5,
		"G": 6,
		"H": 7,
		"I": 8,
		"J": 9,
	}
	letter := (*ci)[:1]
	letterIdx := letterIds[string(letter)]
	return letterIdx
}

func (ci *CellId) getNumberIdx() int {
	numberIdx, err := strconv.Atoi(string((*ci)[1:]))
	if err != nil {
		panic("make shure error will never occured")
	}
	return numberIdx - 1
}

func coordinatesToString(letterI, numberI int) string {
	letters := []string{"A","B","C","D","E","F","G","H","I","J"}
	return letters[letterI] + strconv.Itoa(numberI + 1)
}

func (c *Cell) waitForShotState() bool {
	return c.State == WAIT_FOR_SHOT_CELL_STATE
}

func (c *Cell) setWaitForShotState() {
	c.State = WAIT_FOR_SHOT_CELL_STATE
	return
}

func (c *Cell) overshotState() bool {
	return c.State == OVERSHOT_CELL_STATE
}

func (c *Cell) setOvershotState() {
	c.State = OVERSHOT_CELL_STATE
	return
}

func (c *Cell) hitState() bool {
	return c.State == HIT_SHIP_CELL_STATE
}

func (c *Cell) setHitState() {
	c.State = HIT_SHIP_CELL_STATE
	return
}

func newCell() *Cell {
	return &Cell {
		State: WAIT_FOR_SHOT_CELL_STATE,
		IsNeerShip: false,
		Ship: "",
	}
}

func (c *Cell) isEmpty() bool {
	return c.Ship == ""
}

func (c *Cell) isNeerShip() bool {
	return c.IsNeerShip
}

func (c *Cell) setNeerShip() {
	c.IsNeerShip = true
	return
}

//
// SHIP METHODS
//
func isAllSank(ships Ships) bool {
	for _, ship := range ships {
		if ship.Size != ship.Hits {
			return false
		}
	}
	return true
}

func randomlySetShip(ship *Ship, ships *Ships, field *Field) int {
	letterI, numI := getRandomCoordinates()
	direction := rand.Intn(4)
	if field.isFree(letterI, numI, direction, ship.Size) {

		field.setShip(ship, letterI, numI, direction)

		return 0
	} else {
		return randomlySetShip(ship, ships, field)
	}
}

func coordinatesValid(letterI, numI int) bool {
	if letterI < 0 {
		return false
	}
	if letterI > 9 {
		return false
	}
	if numI < 0 {
		return false
	}
	if numI > 9 {
		return false
	}
	return true
}

func newShips() Ships {
	var shipsToInit = [10] int {1,1,1,1,2,2,2,3,3,4}
	ships := make(map[ShipId] *Ship, len(shipsToInit))
	for _, size := range shipsToInit {
		u, err := uuid.NewV4()
		if err != nil {
			panic(err)
		}
		uuidString := u.String()
		shipId := ShipId(uuidString)
		ships[shipId] = newShip(shipId, size)
	}
	return ships
}

func newShip(shipId ShipId, shipSize int) *Ship {
	return &Ship {
		Id: shipId,
		Size: shipSize,
		Hits: 0,
	}
}


func getRandomCoordinates() (letterI, numI int) {
	letterI = rand.Intn(10)
	numI = rand.Intn(10)
	return
}

func (s *Ship) isSank() bool {
	return s.Hits == s.Size
}
