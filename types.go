package main

type UserId int
type UserState int
const (
	IDLE_USER_STATE UserState = 0
	PLAY_USER_STATE UserState = 1
	WAIT_RAND_PLAYER_USER_STATE = 2
)

type User struct {
	id UserId
	WinsCnt int
	LoseCnt int
	LeaveCnt int
	Score int
	ChatId int
	State UserState
	GameId GameId
	FirstName string
	LastName string
	Username string
}
