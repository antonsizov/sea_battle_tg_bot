package main

const startMsg = "To start game click /play_bot or /help to list all available commands or use keyboard menu ⬇️⬇️⬇️"

// HELP MENU
const playWithBotCmd = "play with bot"
const playWithRandUserCmd = "play with random user"
const playWithFriendCmd = "play with friend"
const cancelOperationCmd = "cancel current operation"
const meCmd = "show my profile statistic"
const ranksCmd = "show global ranks"
const creditCmd = "who created this game?"
const rateCmd = "rate this game"
const feedbackCmd = "send message to developers"
const helpCmd = "show commands list"

// Main menu
const playWithBotMenu = "1. Play with bot"
const playWithRandPlayerMenu = "2. Play with random player"
const helpMenu = "2. Help"

// Messages
const completeCurrentActionFirstMsg = "Complete current action or cancel it first"
const waitForRandPlayerMsg = "Waiting for random palayer to start new game..."
const somethingGoWrongMsg = "Something goes wrong, Sorry and try later"
const notImplementedMsg = "Sorry, it is not implemented yet 😢\nClick /help to view all available commands or use keyboard menu ⬇️⬇️⬇️"

const creditsMsg = "This bot is created by @antonfr"

const gameAlreadyExsistMsg = "Game is already exist, use /help to show commands list or use keyboard menu ⬇️⬇️⬇️"
const gameStartMsg = "We randomly set you ships. You shot the first. Lets start. Select cell to shot ✅"
const gameStopMsg = "Game has stopped, use /help to show commands list or use keyboard menu ⬇️⬇️⬇️"
const youHaveNotGameMsg = "You currently have not game, use /help to show commands list or use keyboard menu ⬇️⬇️⬇️"
const selectCoordinatesMsg = "Select coordinates to shot!"
const notYourTurnMsg = "It is not your turn now. Please wait 🚫"
const invalidCoordinatesMsg = "Position is invalid. Please, retry ⚠️"
const cellNotAvailableMsg = "Cell is not available for shot. Try again ⚠️"

const youOvershotMsg = "Your shots on map\n%s\n\nYou overshot. Opponent's turn. Please wait 🚫"
const opponentHitYourShipMsg = "Your ships state:\n%s\n\nOpponent hit your ship - %s. Its turn again. Please wait 🚫"
const opponentSankYourShipMsg = "Your ships state:\n%s\n\nOpponent sank your ship - %s. Its turn again. Please wait 🚫"
const opponentOverShotMsg = "Opponent overshot - %s. Your turn! ✅"
const opponentWonMsg = "Your ships state:\n%s\n\nOpponent won - %s. And you lose. Click /start to try again!"
const youHitShipMsg = "Your shots on map\n%s\n\nYou hit in the ship. You turn again - try to sink the ship ✅"
const youSankShipMsg = "Your shots on map\n%s\n\nYou sink the ship! Congrats! Try to hit the next one! ✅"
const youWinMsg = "Your shots on map\n%s\n\nYou win! Congrats! Use keyboard menu to start new game ⬇️⬇️⬇️"

var emojiNumbers = []string{
		"1⃣",
		"2⃣",
		"3⃣",
		"4⃣",
		"5⃣",
		"6⃣",
		"7⃣",
		"8⃣",
		"9⃣",
		"🔟",
	}

const overshotCellSymbol = "✔️"
const hitCellSymbol = "💥"
const shipCellSymbol = "⛵️"
const waitForShotCellSymbol = "❔"
