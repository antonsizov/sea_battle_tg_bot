package main

import (
	"strconv"
	"fmt"
	"encoding/json"
	"sync"
	"errors"

	"github.com/syndtr/goleveldb/leveldb"
	"github.com/rockneurotiko/go-tgbot"
)

var usersUpdateMu = &sync.Mutex{}
var appDB *leveldb.DB

//
// API
//

// get user or create
func getUserOrCreate(msg tgbot.Message) (user *User, err error) {
	userId := UserId(msg.From.ID)
	user, err = getUserById(userId)
	if err == leveldb.ErrNotFound {
		return createUser(msg)
	}
	return
}

func (u *User) setWaitForRandPlayerState() (err error) {
	usersUpdateMu.Lock()
	defer usersUpdateMu.Unlock()

	// get user
	// if any error - return
	user, err := getUserById(u.id)
	if err != nil {
		return err
	}

	if user.State != IDLE_USER_STATE {
		return errors.New("user not in idle state")
	}

	user.State = WAIT_RAND_PLAYER_USER_STATE

	json, err := json.Marshal(user)
	if err != nil {
		return err
	}
	err = appDB.Put(getUserKey(user.id), json, nil)
	return
}

func (u *User) setUserStatePlay(gameId GameId) (err error) {
	usersUpdateMu.Lock()
	defer usersUpdateMu.Unlock()

	// get user
	// if any error - return
	user, err := getUserById(u.id)
	if err != nil {
		return err
	}

//	if user.State != IDLE_USER_STATE {
//		return errors.New("user not in idle state")
//	}

	user.State = PLAY_USER_STATE
	user.GameId = gameId

	json, err := json.Marshal(user)
	if err != nil {
		return err
	}
	err = appDB.Put(getUserKey(user.id), json, nil)
	return
}

func (u *User) setUserStateIdle() (err error) {
	usersUpdateMu.Lock()
	defer usersUpdateMu.Unlock()

	// get user
	// if any error - return
	user, err := getUserById(u.id)
	if err != nil {
		return err
	}

	// TODO: set counter, check current state
	user.State = IDLE_USER_STATE
	user.GameId = ""

	json, err := json.Marshal(user)
	if err != nil {
		return err
	}
	fmt.Println("setUserGame: ", string(json))
	err = appDB.Put(getUserKey(u.id), json, nil)
	return
}

//
// Internals - do not use below!
//

// get user by userId
func getUserById(userId UserId) (user *User, err error) {
	key := getUserKey(userId)
	data, err := appDB.Get(key, nil)
	if err != nil {
		return nil, err
	}
	user = &User{}
	err = json.Unmarshal(data, user)
	if err != nil {
		return
	}
	user.id = userId
	return
}

func createUser(msg tgbot.Message) (user *User, err error) {
	usersUpdateMu.Lock()
	defer usersUpdateMu.Unlock()

	// check that user does not exist
	// if exist or error - return
	user, err = getUserById(UserId(msg.From.ID))
	if err != leveldb.ErrNotFound {
		return user, err
	}

	// if user not exist - create new one
	userId := UserId(msg.From.ID)

	lastName := ""
	if msg.From.LastName != nil {
		lastName = *msg.From.LastName
	}

	username := ""
	if msg.From.Username != nil {
		username = *msg.From.Username
	}

	user = &User{
		id: userId,
		WinsCnt: 0,
		LoseCnt: 0,
		LeaveCnt: 0,
		Score: 0,
		ChatId: msg.Chat.ID,
		State: IDLE_USER_STATE,
		GameId: "",
		FirstName: msg.From.FirstName,
		LastName: lastName,
		Username: username,
	}
	json, err := json.Marshal(user)
	if err != nil {
		return nil, err
	}
	fmt.Println("user json: ", string(json))
	err = appDB.Put(getUserKey(userId), json, nil)
	return
}

func getUserKey(userId UserId) []byte {
	return []byte("users" + strconv.Itoa(int(userId)))
}
