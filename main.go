package main

// TODO: provide Cancel key to abort current game
// TODO: make it possible to skip get letter step
// TODO: remove unavailable cells from keyboard
// TODO: refactor states with state methods
// TODO: move all text msgs into language file
// TODO: refactor random coordinates to eliminate stack overflow
// TODO: think about unite field and ships into one object

import (
	"time"
	"bytes"
	"fmt"
	"math/rand"
	"os"
	"os/signal"
	"syscall"
	"strconv"

	"github.com/joho/godotenv"
	"github.com/nu7hatch/gouuid"
	"github.com/rockneurotiko/go-tgbot"
	"github.com/syndtr/goleveldb/leveldb"
)

var instagramid = ""

type Command struct {
	cmd string
	dscr string
	avl bool
}

var allCommands = []Command{
	Command{cmd: "/play_bot",     dscr: playWithBotCmd,      avl: true},
	Command{cmd: "/play_user",    dscr: playWithRandUserCmd, avl: true},
	Command{cmd: "/play_friend",  dscr: playWithFriendCmd,   avl: false},
	Command{cmd: "/cancel",       dscr: cancelOperationCmd,  avl: true},
	Command{cmd: "/me",           dscr: meCmd,               avl: false},
	Command{cmd: "/ranks",        dscr: ranksCmd,            avl: false},
	Command{cmd: "/credits",      dscr: creditCmd,           avl: true},
	Command{cmd: "/rate",         dscr: rateCmd,             avl: false},
	Command{cmd: "/feedback",     dscr: feedbackCmd,         avl: false},
	Command{cmd: "/help",         dscr: helpCmd,             avl: true},
}

func buildHelpMessage(showOnlyAvailable bool) string {
	var buffer bytes.Buffer
	for _, cmd := range allCommands {
		str := ""

		if showOnlyAvailable && cmd.avl {
			str = fmt.Sprintf("%s - %s\n", cmd.cmd, cmd.dscr)
		} else if !showOnlyAvailable {
			str = fmt.Sprintf("%s - %s\n", cmd.cmd[1:], cmd.dscr)
		}
		buffer.WriteString(str)
	}
	return buffer.String()
}

func creditsHandler(bot tgbot.TgBot, msg tgbot.Message, text string) *string {
	res := creditsMsg
	return &res
}

func helpHandler(bot tgbot.TgBot, msg tgbot.Message, text string) *string {
	res := buildHelpMessage(true)
	return &res
}

func botfatherHelpHandler(bot tgbot.TgBot, msg tgbot.Message, text string) *string {
	res := buildHelpMessage(false)
	return &res
}

func startHandler(bot tgbot.TgBot, msg tgbot.Message, text string) *string {
	return replyMainMenuMsg(bot, msg, startMsg)
}

func getMainMenuKeyboard() tgbot.ReplyKeyboardMarkup {
	keylayout := [][]string{
		{playWithBotMenu},
		{playWithRandPlayerMenu},
		{helpMenu},
	}
	rkm := tgbot.ReplyKeyboardMarkup{
		Keyboard:        keylayout,
		ResizeKeyboard:  false,
		OneTimeKeyboard: true,
		Selective:       false}
	return rkm
}

func playWithRandPlayerHandler(bot tgbot.TgBot, msg tgbot.Message, text string) *string {
	msgr := ""
	user, err := getUserOrCreate(msg)
	if err != nil {
		msgr = somethingGoWrongMsg
		return &msgr
	}
	if user.State != IDLE_USER_STATE {
		msgr = completeCurrentActionFirstMsg
		return &msgr
	}

	if waitForRandomPlayer == nil {
		waitForRandomPlayer = &user.id
		user.setWaitForRandPlayerState()
		msgr = waitForRandPlayerMsg
		return &msgr
	}


	// get user2
	// TODO: wrap getuser & update user into lock
	user2, err := getUserById(*waitForRandomPlayer)
	waitForRandomPlayer = nil // lock waitForRandPlayerMsg variable
	if err != nil {
		msgr = somethingGoWrongMsg
		return &msgr
	}
	if user2.State != WAIT_RAND_PLAYER_USER_STATE {
		msgr = somethingGoWrongMsg
		return &msgr
	}


	game := newGame(user.id, user2.id)
	u, err := uuid.NewV4()
	if err != nil {
		msgr = somethingGoWrongMsg
		return &msgr
	}
	gameId := GameId(u.String())

	gamesMap[gameId] = game
	game.setUserShips()
	userField := game.getUserFieldASCIIState(user.id)
	user.setUserStatePlay(gameId)
	user2.setUserStatePlay(gameId)

	OpponentFieldASCIIState := game.getUserFieldASCIIState(user2.id)
	fmt.Println("opponent field:\n", OpponentFieldASCIIState)
	// send opponent notification that game started

	yourGameStartedMsg := "Your game started, wait for your opponent shot...\n\n" + OpponentFieldASCIIState
	bot.SendMessage(user2.ChatId, yourGameStartedMsg, nil, nil, nil)
	//ResultWithMessage

	msgr = gameStartMsg + "\n\n" + userField
	fmt.Println("/start: ", msgr)

	return replyYourTurn(bot, msg, msgr)
}

func playWithBotHandler(bot tgbot.TgBot, msg tgbot.Message, text string) *string {
	msgtext := ""
	user, err := getUserOrCreate(msg)
	if err != nil {
		msgtext = somethingGoWrongMsg
		return &msgtext
	}
	if user.State == PLAY_USER_STATE {
		msgtext = gameAlreadyExsistMsg
		return &msgtext
	}

	game := newGame(user.id, BOT_USER_ID)
	u, err := uuid.NewV4()
	if err != nil {
		msgtext = somethingGoWrongMsg
		return &msgtext
	}
	uuidString := u.String()
	gameId := GameId(uuidString)

	gamesMap[gameId] = game
	game.setUserShips()
	userField := game.getUserFieldASCIIState(user.id)
	user.setUserStatePlay(gameId)

	// for debug only
	OpponentFieldASCIIState := game.getOpponentFieldASCIIState(user.id)
	fmt.Println("opponent field:\n", OpponentFieldASCIIState)

	msgtext = gameStartMsg + "\n\n" + userField
	fmt.Println("/start: ", msgtext)

	return replyYourTurn(bot, msg, msgtext)
}

func replyMainMenuMsg(bot tgbot.TgBot, msg tgbot.Message, msgtext string) *string {
	bot.Answer(msg).Text(msgtext).Keyboard(getMainMenuKeyboard()).End()
	return nil
}

func notImplementedHandler(bot tgbot.TgBot, msg tgbot.Message, msgtext string) *string {
	bot.Answer(msg).Text(notImplementedMsg).Keyboard(getMainMenuKeyboard()).End()
	return nil
}

func getCharsKeyboard() tgbot.ReplyKeyboardMarkup {
	keylayout := [][]string{
		{"A", "B", "C", "D", "E"},
		{"F", "G", "H", "I", "J"},
		{"Cancel"},
	}
	rkm := tgbot.ReplyKeyboardMarkup{
		Keyboard:        keylayout,
		ResizeKeyboard:  false,
		OneTimeKeyboard: true,
		Selective:       false}
	return rkm
}

func replyYourTurn(bot tgbot.TgBot, msg tgbot.Message, msgtext string) *string {
	bot.Answer(msg).Text(msgtext).Keyboard(getCharsKeyboard()).End()
	return nil
}

func cancelHandler(bot tgbot.TgBot, msg tgbot.Message, text string) *string {
	msgtext := ""

	user, err := getUserOrCreate(msg)
	if err != nil {
		msgtext = somethingGoWrongMsg
		return &msgtext
	}

	if user.State == IDLE_USER_STATE {
		return replyMainMenuMsg(bot, msg, gameStopMsg)
	}

	_, ok := gamesMap[user.GameId]
	user.setUserStateIdle()
	if(ok) {
		delete(gamesMap, user.GameId)
		return replyMainMenuMsg(bot, msg, gameStopMsg)
	}

	return replyMainMenuMsg(bot, msg, youHaveNotGameMsg)
}

func allMsgHand(bot tgbot.TgBot, msg tgbot.Message) {
	fmt.Printf("Received message: %+v\n", msg)
}

func charHandler(bot tgbot.TgBot, msg tgbot.Message, char string) *string {
	msgr := ""

	user, err := getUserOrCreate(msg)
	if err != nil {
		msgr = somethingGoWrongMsg
		return &msgr
	}

	if user.State != PLAY_USER_STATE {
		return replyMainMenuMsg(bot, msg, youHaveNotGameMsg)
	}

	game, ok := gamesMap[user.GameId]

	if !ok {
		return replyMainMenuMsg(bot, msg, youHaveNotGameMsg)
		// TODO: reset user state
	}

	if !game.isUserTurn(user.id) {
		msgr = notYourTurnMsg
		return &msgr
	}


	keylayout := make([][]string, 3)
	for i := range keylayout {
		keylayout[i] = make([]string, 5)
	}


	var i,j int
	i = 0
	keylayout[i] = make([]string, 5)
	for j = 0; j < 5; j++ {
		keylayout[i][j] = char + strconv.Itoa(((i*5) + j + 1))
	}
	i = 1
	keylayout[i] = make([]string, 5)
	for j = 0; j < 5; j++ {
		keylayout[i][j] = char + strconv.Itoa(((i*5) + j + 1))
	}
	i = 2
	keylayout[i] = make([]string, 1)
	keylayout[i][0] = "Cancel"

	rkm := tgbot.ReplyKeyboardMarkup{
		Keyboard:        keylayout,
		ResizeKeyboard:  false,
		OneTimeKeyboard: true,
		Selective:       false}
	bot.Answer(msg).Text(selectCoordinatesMsg).Keyboard(rkm).End()

	return nil
}

func charAndNumHandler(bot tgbot.TgBot, msg tgbot.Message, position string) *string {
	msgr := ""

	user, err := getUserOrCreate(msg)
	if err != nil {
		msgr = somethingGoWrongMsg
		return &msgr
	}

	if user.State != PLAY_USER_STATE {
		return replyMainMenuMsg(bot, msg, youHaveNotGameMsg)
	}

	game, ok := gamesMap[user.GameId]

	// check game exist
	if !ok {
		return replyMainMenuMsg(bot, msg, youHaveNotGameMsg)
		// TODO: reset user state
	}

	// convert position and check numI
	letterString := position[:1]
	letterIdx, err := getLetterIdx(letterString)
	// NOTE: in case of Char not in range, request not handled in
	// this function because of routing regexp
	if err != nil {
		return replyYourTurn(bot, msg, invalidCoordinatesMsg)
	}

	numberIdxString := position[1:]
	numberIdx, err := strconv.Atoi(numberIdxString)
	numberIdx--
	if err != nil {
		return replyYourTurn(bot, msg, invalidCoordinatesMsg)
	}
	if numberIdx < 1 && numberIdx > 10 {
		return replyYourTurn(bot, msg, invalidCoordinatesMsg)
	}

	if !game.isUserTurn(user.id) {
		msgr = notYourTurnMsg
		return &msgr
	}

	if !game.isOpponentCellAvailableForShot(user.id, letterIdx, numberIdx) {
		return replyYourTurn(bot, msg, cellNotAvailableMsg)
	}

	switch game.makeUserShot(user.id, letterIdx, numberIdx) {
	case OVERSHOT_SHOT_RESULT:
		msgr = fmt.Sprintf(youOvershotMsg, game.getUserShotsASCIIMap(user.id))
		bot.Answer(msg).Text(msgr).End()
		handleOpponentTurn(bot, msg, game, user, position)
		return nil
	case HIT_SHIP_SHOT_RESULT:
		msgr = fmt.Sprintf(youHitShipMsg, game.getUserShotsASCIIMap(user.id))
		handleOpponentHitYourShip(bot, game, user)
		return replyYourTurn(bot, msg, msgr)
	case SANK_SHIP_SHOT_RESULT:
		msgr = fmt.Sprintf(youSankShipMsg, game.getUserShotsASCIIMap(user.id))
		handleOpponentSankYourShip(bot, game, user)
		return replyYourTurn(bot, msg, msgr)
	case GAME_OVER_SHOT_RESULT:
		msgr = fmt.Sprintf(youWinMsg, game.getUserShotsASCIIMap(user.id))
		handleOpponentWon(bot, game, user)
		delete(gamesMap, user.GameId);
		user.setUserStateIdle()
		return replyMainMenuMsg(bot, msg, msgr)
	}

	return nil
}

func handleOpponentHitYourShip(bot tgbot.TgBot, game *Game, user *User) {
	msg := "Opponent hit your ship :( , his turn again..."
	sendOpponentMsg(bot, game, user, msg, nil)
	return
}

func handleOpponentSankYourShip(bot tgbot.TgBot, game *Game, user *User) {
	msg := "Opponent sank your ship :( , his turn again..."
	sendOpponentMsg(bot, game, user, msg, nil)
	return
}

func handleOpponentWon(bot tgbot.TgBot, game *Game, user *User) {
	msg := "Opponent won this game :( , good luck next time"
	sendOpponentMsg(bot, game, user, msg, nil)
	return
}

func sendOpponentMsg(bot tgbot.TgBot, game *Game, user *User, msg string, rkm *tgbot.ReplyKeyboardMarkup) {
	opponentUserId := game.getOpponentUserId(user.id)
	if opponentUserId == BOT_USER_ID {
		return
	}
	OpponentFieldASCIIState := game.getOpponentFieldASCIIState(user.id)
	msg = OpponentFieldASCIIState + "\n\n" + msg
	opponentUser, err := getUserById(opponentUserId)
	if err != nil {
		// TODO: log here
		return
	}

	if rkm == nil {
		bot.SendMessage(opponentUser.ChatId, msg, nil, nil, nil)
	} else {
		bot.SendMessageWithKeyboard(opponentUser.ChatId, msg, nil, nil, *rkm)
	}
	return
}

func handleOpponentTurn(bot tgbot.TgBot, msg tgbot.Message, game *Game, user *User, pos string) {
	opponentUserId := game.getOpponentUserId(user.id)
	if opponentUserId == BOT_USER_ID {
		botShotHandler(bot, msg, game, user)
	} else {
		rkm := getCharsKeyboard()
		msgToOp := fmt.Sprintf(opponentOverShotMsg, pos)
		sendOpponentMsg(bot, game, user, msgToOp, &rkm)
	}

	return
}

func botShotHandler(bot tgbot.TgBot, msg tgbot.Message, game *Game, user *User) {
	msgr := ""
	var cellId CellId
	for botShot := HIT_SHIP_SHOT_RESULT; botShot > OVERSHOT_SHOT_RESULT; {
		time.Sleep(1500 * time.Millisecond)
		botShot, cellId = game.makeBotShot()
		botShotCoordinatesString := string(cellId)
		if botShot == HIT_SHIP_SHOT_RESULT {
			msgr = fmt.Sprintf(opponentHitYourShipMsg, game.getUserFieldASCIIState(user.id), botShotCoordinatesString)
			bot.Answer(msg).Text(msgr).End()
		} else if botShot == SANK_SHIP_SHOT_RESULT {
			msgr = fmt.Sprintf(opponentSankYourShipMsg, game.getUserFieldASCIIState(user.id), botShotCoordinatesString)
			bot.Answer(msg).Text(msgr).End()
		} else if botShot == OVERSHOT_SHOT_RESULT {
			msgr = fmt.Sprintf(opponentOverShotMsg, botShotCoordinatesString)
			replyYourTurn(bot, msg, msgr)
		} else if botShot == GAME_OVER_SHOT_RESULT {
			msgr = fmt.Sprintf(opponentWonMsg, game.getUserFieldASCIIState(user.id), botShotCoordinatesString)
			delete(gamesMap, user.GameId);
			user.setUserStateIdle()
			bot.Answer(msg).Text(msgr).End()
		}
	}
	return
}

func cleanup() {
	fmt.Printf("stopping...")
	appDB.Close()
	fmt.Println("done")
}

func main() {

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	signal.Notify(c, syscall.SIGTERM)
	go func() {
		<-c
		cleanup()
		os.Exit(1)
	}()

	rand.Seed( time.Now().UTC().UnixNano())
	db, err := leveldb.OpenFile("data/", nil)
	appDB = db
	if err != nil {
		panic(fmt.Sprintf("Open db error: %v", err))
	}
	gamesMap = make(map[GameId] *Game)
	godotenv.Load("secrets.env")
	token := os.Getenv("TELEGRAM_KEY")
	instagramid = os.Getenv("INSTAGRAM_CLIENT_ID")

	bot := tgbot.NewTgBot(token).
		SimpleCommandFn(`start`, startHandler).

		SimpleCommandFn(`play_bot`, playWithBotHandler).
		SimpleRegexFn(`^1. Play with bot$`, playWithBotHandler).

		SimpleCommandFn(`help`, helpHandler).
		SimpleRegexFn(`^2. Help$`, helpHandler).

		SimpleCommandFn(`play_user`, playWithRandPlayerHandler).
		SimpleRegexFn("^2. Play with random player$", playWithRandPlayerHandler).

		SimpleCommandFn(`/botfather`, botfatherHelpHandler).
		SimpleCommandFn(`/credits`, creditsHandler).
		SimpleCommandFn(`cancel`, cancelHandler).
		SimpleRegexFn(`^Cancel$`, cancelHandler).

		SimpleCommandFn(`play_friend`, notImplementedHandler).
		SimpleCommandFn(`me`, notImplementedHandler).
		SimpleCommandFn(`ranks`, notImplementedHandler).
		SimpleCommandFn(`rate`, notImplementedHandler).
		SimpleCommandFn(`feedback`, notImplementedHandler).

		SimpleRegexFn(`^[a-jA-J]$`, charHandler).
		SimpleRegexFn(`^[a-jA-J][\d]{1,2}$`, charAndNumHandler).
		AnyMsgFn(allMsgHand)


    // Disable all link preview by default
	bot.DefaultDisableWebpagePreview(true)
	// Enable one time keyboard by default
	bot.DefaultOneTimeKeyboard(true)
	// Use Seletive by default
	bot.DefaultSelective(true)
	// By default is true! (This removes initial @username from messages)
	bot.DefaultCleanInitialUsername(true)
	// By default is true! (This adds the / in the messages that
	// have @username, this needs DefaultCleanInitialUsername true,
	// for example: @username test becomes /test)
	bot.DefaultAllowWithoutSlashInMention(true)

	fmt.Printf("bot started\n")
	bot.SimpleStart()
}
